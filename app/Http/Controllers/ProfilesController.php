<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
    //Получение модели пользователя с ее отношениями позволяет делать меньше запросов и таскать за собой меньше данных
    public function index($user)
    {
        $user =User::with('posts', 'profile')->find($user);
        return view('profiles.index', [
            'user'=>$user
        ]);
    }

    public function edit($user){
            $user =User::with('profile')->find($user);
            $this->authorize('update', $user->profile);
            return view('profiles.edit', [
                'user'=>$user
            ]);
    }


    public function update($user){
        $user = User::with('profile')->find($user);
        $this->authorize('update', $user->profile);
        $data = request()->validate([
            'title'=>'required',
            'description'=>'required',
            'url'=>'url',
            'image'=>'',
        ]);

        if(request('image')){
           $imagePath = request('image')->store('profiles', 'public');

           $image = Image::make(public_path("profiles/{$imagePath}"))-fit(1000,1000);
           $image->save();
        }

        dd($data);

        auth()->user()->profile->update(array_merge(
            $data,
            ['image' => $imagePath]
        ));

        return redirect("/profile/{$user->id}");

    }

//    public function update(User $user){
//        $data = request()->validate([
//            'title'=>'required',
//            'description'=>'required',
//            'url'=>'url',
//            'image'=>'',
//        ]);
//
//        auth()->user()->profile->update($data);
//
//        return redirect("/profile/{$user->id}");
//
//    }
}
