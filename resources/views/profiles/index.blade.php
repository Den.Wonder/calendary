<?php

date_default_timezone_set("Asia/Krasnoyarsk");
$today = date('j-m-Y', time());

$num_of_month = date('m',time());
$input_date = '2019-09-22';
$input_year =  date('Y',time());
$choose_day = date('j', time());



if(isset($_GET['num_of_month'])){
    $num_of_month = $_GET['num_of_month'];
}
if(isset($_GET['input_year'])){
    $input_year = $_GET['input_year'];
}

if(isset($_POST['choose_day'])){
    $choose_day=$_POST['choose_day'];
    $input_date = $input_year."-".$num_of_month."-".$choose_day;
    echo $input_date;
}



if (isset($_POST['input_month'])&& isset($_POST['input_year'])) {
    $input_year=(int)$input_year;

    $input_month = $_POST['input_month'];
    $input_year = $_POST['input_year'];
    $year_is_int = filter_var($input_year, FILTER_VALIDATE_INT);
    if ($year_is_int === false || empty($input_year)||($input_year<0)) {
        echo 'enter correct year value, bitch!!!';
        return false;
    } else{
        $input_month = $_POST['input_month'];
        $input_year = $_POST['input_year'];
    }
    $num_of_month = $input_month;

}




function draw_calendar($month, $year, $action='none'){
    $calendar = '<table class="table table-bordered">';
    $running_day = date('w',mktime(0,0,0,$month,1,$year));
    $running_day --;
    if($running_day == -1){
        $running_day = 6;
    }
    $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
    $day_counter = 0;
    $days_in_this_week = 1;
    $calendar.= '<tr class="b-calendar__row">';

    // вывод пустых ячеек
    for ($x = 0; $x < $running_day; $x++) {
        $calendar.= '<td class="b-calendar__np shufut"></td>';
        $days_in_this_week++;
    }

    // дошли до чисел, будем их писать в первую строку
    for($list_day = 1; $list_day <= $days_in_month; $list_day++) {
        $calendar.= '<td class="b-calendar__day';

        // выделяем выходные дни
        if ($running_day != 0) {
            if (($running_day % 5 == 0) || ($running_day % 6 == 0)) {
                $calendar .= ' b-calendar__weekend';
            }
        }
        $calendar .= '">';

        // пишем номер в ячейку
        $calendar.= '<div class="b-calendar__number"><input type="submit" name="choose_day" value='."$list_day".'>

</div>';
        $calendar.= '</td>';

        // дошли до последнего дня недели
        if ($running_day == 6) {
            // закрываем строку
            $calendar.= '</tr>';
            // если день не последний в месяце, начинаем следующую строку
            if (($day_counter + 1) != $days_in_month) {
                $calendar.= '<tr class="b-calendar__row">';
            }
            // сбрасываем счетчики
            $running_day = -1;
            $days_in_this_week = 0;
        }

        $days_in_this_week++;
        $running_day++;
        $day_counter++;
    }

    // выводим пустые ячейки в конце последней недели
    if ($days_in_this_week < 8) {
        for($x = 1; $x <= (8 - $days_in_this_week); $x++) {
            $calendar.= '<td class="b-calendar__np shufut"></td>';
        }
    }
    $calendar.= '</tr>';
    $calendar.= '</table>';

    return $calendar;
}

if($num_of_month==13){
    $num_of_month=1;
    $input_year++;
}


if($num_of_month==0){
    $num_of_month=12;
    $input_year--;
}

switch ($num_of_month){
    case 1:
        $this_month_title = "Январь";
        break;
    case 2:
        $this_month_title = "Февраль";
        break;
    case 3:
        $this_month_title = "Март";
        break;
    case 4:
        $this_month_title = "Апрель";
        break;
    case 5:
        $this_month_title = "Май";
        break;
    case 6:
        $this_month_title = "Июнь";
        break;
    case 7:
        $this_month_title = "Июль";
        break;
    case 8:
        $this_month_title = "Август";
        break;
    case 9:
        $this_month_title = "Сентябрь";
        break;
    case 10:
        $this_month_title = "Октябрь";
        break;
    case 11:
        $this_month_title = "Ноябрь";
        break;
    case 12:
        $this_month_title = "Декабрь";
        break;
}



$prev = (int)$num_of_month;
$prev --;
$next = (int)$num_of_month;
$next ++;
?>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            <img src="/img/ava_1.jpg" style="max-height: 250px" class="rounded-circle p-5">
        </div>
        <div class="col-9 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <h1>
                    {{ $user->username }}
                </h1>
                @can('update', $user->profile)
                    <a href="/p/create" title="Add new post">Add new post</a>
                @endcan
            </div>
            @can('update', $user->profile)
                <a href="/profile/{{$user->id}}/edit" title="Add new post">Edit profile</a>
            @endcan
            <div class="d-flex">
                <div class="pr-5"><strong>{{$user->posts->count()}}</strong> notes</div>
                <div class="pr-5"><strong>21 k.</strong> followers</div>
                <div class="pr-5"><strong>212</strong> following</div>
            </div>
            <div class="pt-4">
                <b>{{$user->profile->title}}</b>
            </div>
            <div>
                {{$user->profile->description}}
            </div>
            <div>
                <a href="/">{{$user->profile->url ?? 'N/A'}}</a>
            </div>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-8">
{{--            <p>here is calendary</p>--}}
{{--            <form  method="post">--}}
{{--                @csrf--}}
{{--                <div class="form-group">--}}
{{--                    <label for="inputDate">enter date:</label>--}}
{{--                    <input type="date" class="form-control" name="input_date">--}}

{{--                    <input type="submit">--}}
{{--                </div>--}}
{{--            </form>--}}

            <div class="container">
                <div class="month">
                    <h2>
                        <?php echo $input_year ?>
                    </h2>
                    <h2>
                    <form method="post">
                        <input type="submit" value="">
                        <?php echo '<a href="index.blade.php?num_of_month='.$prev.'&input_year='.$input_year.'"> < </a>'; ?>
                        <?php echo $this_month_title ?>
                        <?php echo '<a href="index.blade.php?num_of_month='.$next.'&input_year='.$input_year.'"> > </a>'; ?>
                    </form>
                    </h2>
                </div>
                <div class="calendar">

                    <table class="table table-bordered">
                        <tr>
                            <th>ПН</th>
                            <th>ВТ</th>
                            <th>СР</th>
                            <th>ЧТ</th>
                            <th>ПТ</th>
                            <th>СБ</th>
                            <th>ВС</th>
                        </tr>
                    </table>
                    <form method="post">
                        @csrf
                        <?php
                        echo draw_calendar($num_of_month,$input_year);
                        ?>
                    </form>
                </div>
                <form method="post">
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Месяц</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="input_month">
                            <option value="1">Январь</option>
                            <option value="2">Февраль</option>
                            <option value="3">Март</option>
                            <option value="4">Апрель</option>
                            <option value="5">Май</option>
                            <option value="6">Июнь</option>
                            <option value="7">Июль</option>
                            <option value="8">Август</option>
                            <option value="9">Сентябрь</option>
                            <option value="10">Октябрь</option>
                            <option value="11">Ноябрь</option>
                            <option value="12">Декабрь</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Год</label>
                        <input type="text" class="form-control" name="input_year" placeholder="Введите год">
                    </div>
                    <p><input type="submit" value="Показать календарь"/> </p>
                </form>
            </div>

        </div>
        <div class="col-4">



            @foreach($user->posts as $post)
            @if($post->date == $input_date && $post->private == 0)

                <div class="col-12 p-2" style="border: .5px solid black; margin: 5px  ">
                    <h2>{{$post->title}}</h2>
                    <p style="color: #5a6268">{{$post->caption}}</p>
                    <p>{{$post->text}}</p>
                    <i>{{$post->date}}</i>
                </div>

            @endif

                @if($post->date == $input_date && $post->user_id == $user->id && $post->private == 1 )
                    @can('update', $user->profile)
                        <div class="col-12 p-2" style="border: .5px solid black; margin: 5px  ">
                            <h2>{{$post->title}}</h2>
                            <p style="color: #5a6268">{{$post->caption}}</p>
                            <p>{{$post->text}}</p>
                            <i>{{$post->date}}</i>
                        </div>
                    @endcan

                    @endif

{{--                @if($post->date == $input_date && $post->private == 1 && $post->user_id == $user->profile->id)--}}

{{--                    <div class="col-12 p-2" style="border: .5px solid black; margin: 5px  ">--}}
{{--                        <h2>{{$post->title}}</h2>--}}
{{--                        <p style="color: #5a6268">{{$post->caption}}</p>--}}
{{--                        <p>{{$post->text}}</p>--}}
{{--                        <i>{{$post->date}}</i>--}}
{{--                    </div>--}}

{{--                @endif--}}

            @endforeach
        </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection
