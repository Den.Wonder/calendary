<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function create(){
        return view('posts.create');
    }

    public function store(){
        $data = request()->validate([
            'title'=>'required',
            'caption'=>'',
            'text'=>'required',
            'private'=>'required',
            'date'=>''
        ]);

        auth()->user()->posts()->create($data);

//        dd(request()->all()  );

        return redirect('/profile/' .auth()->user()->id);

    }
}
